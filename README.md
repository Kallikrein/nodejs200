# Sfeir School Node.JS 200 jour 1

Démarrer:

```sh
npm i
```

Lancer les tests:

```sh
npm test -- **/ex01.test.js
```

[exercice 1](./01_hello/README.md)  
[exercice 2](./02_module/README.md)  
[exercice 3](./03_fs/README.md)  
[exercice 4](./04_http/README.md)